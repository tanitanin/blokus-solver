
CC = gcc -O2 -I inc
CXX = g++ -O2 -I inc

BINDIR = ./bin/
SRCDIR = ./src/

OBJS = ${sort ${patsubst %.c,%.o,${wildcard $(SRCDIR)*.c}}\
              ${patsubst %.cc,%.o,${wildcard $(SRCDIR)*.cc}}}

all: $(OBJS)
	$(CC) $^ -o $(BINDIR)main

clean:
	rm $(OBJS)
	rm $(BINDIR)*
