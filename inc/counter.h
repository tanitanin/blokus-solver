#pragma once
#ifndef _COUNTER_H
#define _COUNTER_H

#include <sys/time.h>
#include <sys/resource.h>

#ifdef __cplusplus
extern "C" {
#endif


struct counter {
  struct timeval ut_start;
  struct timeval st_start;
  struct timeval ut_stop;
  struct timeval st_stop;
  double ut;
  double st;
};

void start_counter(struct counter *cnt);
void stop_counter(struct counter *cnt);

#ifdef __cplusplus
}
#endif

#endif // _COUNTER_H
