#include "counter.h"
#include <sys/time.h>
#include <sys/resource.h>

void start_counter(struct counter *cnt) {
  struct rusage usage;
  getrusage(RUSAGE_SELF, &usage);
  cnt->ut_start = usage.ru_utime;
  cnt->st_start = usage.ru_stime;
}

void stop_counter(struct counter *cnt) {
  struct rusage usage;
  getrusage(RUSAGE_SELF, &usage);
  cnt->ut_stop = usage.ru_utime;
  cnt->st_stop = usage.ru_stime;
  cnt->ut = (cnt->ut_stop.tv_sec - cnt->ut_start.tv_sec) + (cnt->ut_stop.tv_usec - cnt->ut_start.tv_usec)*1.0E-6;
  cnt->st = (cnt->st_stop.tv_sec - cnt->st_start.tv_sec) + (cnt->st_stop.tv_usec - cnt->st_start.tv_usec)*1.0E-6;
}

