#include <stdio.h>
#include <stdlib.h>
#include "blokus.h"


// positionの初期化
// param pos: 盤面
void clear_position(struct position *pos) {
#ifdef DEBUG
  printf("clear_position addr:%x\n",pos);
#endif

  // プレイヤーを初期化
  int x,y,i;
  pos->myself = (struct player *)malloc(sizeof(struct player));
  pos->myself->id = 0;
  pos->opponent = (struct player *)malloc(sizeof(struct player));
  pos->opponent->id = 1;
  for(i=0;i<PIECE_NUM;++i) {
    pos->myself->used[i] = false;
    pos->opponent->used[i] = false;
  }

  // 盤面上のマスをすべてNONEに
  for(y=0;y<STAGE_SIZE+2;++y) {
    for(x=0;x<STAGE_SIZE+2;++x) {
      set_data(pos,pos->myself,x,y,NONE);
      set_data(pos,pos->opponent,x,y,NONE);
    }
  }
  for(x=0;x<STAGE_SIZE+2;++x) {
    set_data(pos,pos->myself,x,0,NG);
    set_data(pos,pos->myself,x,STAGE_SIZE+1,NG);
    set_data(pos,pos->opponent,x,0,NG);
    set_data(pos,pos->opponent,x,STAGE_SIZE+1,NG);
  }
  for(y=0;y<STAGE_SIZE+2;++y) {
    set_data(pos,pos->myself,0,y,NG);
    set_data(pos,pos->opponent,0,y,NG);
    set_data(pos,pos->myself,STAGE_SIZE+1,y,NG);
    set_data(pos,pos->opponent,STAGE_SIZE+1,y,NG);
  }

  // 手番の最初に置ける場所を指定
  set_data(pos,pos->myself,5,5,OK);
  set_data(pos,pos->opponent,10,10,OK);

  // 手番をすべて0に
  for(i=0;i<PIECE_NUM*2+1;++i) {
    pos->turns[i].player = 0;
    pos->turns[i].piece = 0;
    pos->turns[i].dir = 0;
    pos->turns[i].pos.x = 0;
    pos->turns[i].pos.y = 0;
  }
  pos->turn_pos = 0;
}

// 盤面の状態を更新
// param pos: 盤面
// param pl: プレイヤー
// param x: x座標
// param y: y座標
// param state: 状態
// return: none
void set_data(struct position *pos, struct player *pl, int x, int y, int state) {
  pos->data[(x)*(STAGE_SIZE+2) + (y)] &= ~(0x0f << ((pl->id)*4));
  pos->data[(x)*(STAGE_SIZE+2) + (y)] |= (state << ((pl->id)*4));
  //pos->data[(x+1)*(STAGE_SIZE+2) + (y+1)] &= ~(0x0f << 16);
  //pos->data[(x+1)*(STAGE_SIZE+2) + (y+1)] |= (pl->id << 16);
}

// 盤面の状態を取得
// param pos: 盤面
// param pl: プレイヤー
// param x: x座標
// param y: y座標
// return: state (NONE, SET, NG or OK)
int get_data(struct position *pos, struct player *pl, int x, int y) {
  int data = pos->data[(x)*(STAGE_SIZE+2) + (y)];
  return (data >> ((pl->id)*4)) & 0x0f;
}

// 盤面の外に出ているか
// param pos: 盤面
// param x: x座標
// param y: y座標
// return: true or false
bool is_out(struct position *pos, int x, int y) {
  if(x <= 0 || y <= 0 || STAGE_SIZE < x || STAGE_SIZE < y) {
    return true;
  } else {
    return false;
  }
}

// 重なっていないかチェック
// param pos: 局面
// return: true or false
bool is_overlap(struct position *pos, int piece_num, int piece_dir, int x, int y) {
#ifdef DEBUG
  printf("is_overlap? pos:%x piece:%d dir:%d x:%d y:%d\n",pos,piece_num,piece_dir,x,y);
#endif
  int px,py,dx,dy;
  for(dx=0;dx<PIECE_SIZE;++dx) {
    for(dy=0;dy<PIECE_SIZE;++dy) {
      px = x + dx - PIECE_SIZE/2; py = y + dy - PIECE_SIZE/2;

      // ピースがないマスは関係ない
      if(get_piece(piece_num,piece_dir,dx,dy) != SET) continue;

      // 盤面の外にはみ出ていたらダメ
      if(is_out(pos,px,py)) return true;

      // すでにいずれかのピースが置いてあったら重なっている
      if(get_data(pos,pos->myself,px,py) == SET) return true;
      if(get_data(pos,pos->opponent,px,py) == SET) return true;
    }
  }
  return false;
}

// 点で接しているかチェック
// param pos: 局面
// param pl: プレイヤー
// param piece_num: ピース番号
// param piece_dir: ピースの向き
// param pt: ピースを置く場所
bool is_attached(struct position *pos, struct player *pl, int piece_num, int piece_dir, int x, int y) {
#ifdef DEBUG
  printf("is_attached? pos:%x piece:%d dir:%d x:%d y:%d\n",pos,piece_num,piece_dir,x,y);
#endif
  int px,py,dx,dy,piece_status,pos_status;
  bool flag=false;
  for(dx=0;dx<PIECE_SIZE;++dx) {
    for(dy=0;dy<PIECE_SIZE;++dy) {
      px = x + dx - PIECE_SIZE/2; py = y + dy - PIECE_SIZE/2;
      if(is_out(pos,px,py)) continue;

      piece_status = get_piece(piece_num,piece_dir,dx,dy);
      pos_status = get_data(pos,pl,px,py);
      //printf(" p:(%d,%d) dxy:(%d,%d) piece_status:%d pos_status:%d\n",px,py,dx,dy,piece_status,pos_status);

      // ピースがないマスは関係ない
      if(piece_status != SET) continue;

      // 辺に重ならない・カドに少なくともひとつ重なっている
      if(pos_status == NG) return false;
      if(pos_status == OK) flag = true;
    }
  }
  return flag;
}

// 特定のピースが打てるかどうか
// param pos: 局面
// param pl: プレイヤー
// param piece_num: ピース番号
// param piece_dir: ピースの向き
// param pt: ピースを置く場所
bool ok(struct position *pos, struct player *pl, int piece_num, int piece_dir, int x, int y) {
  // for debug
#ifdef DEBUG
  printf("ok? &pos:%x player_id:%d piece_num:%d piece_dir:%d x:%d y%d:\n",pos,pl->id,piece_num,piece_dir,x,y);
#endif
  //bool ov = is_overlap(pos,piece_num,piece_dir,x,y);
  //bool at = is_attached(pos,pl,piece_num,piece_dir,x,y);
  //printf("p:%d d:%d x:%d y:%d overlap:%d attached:%d\n",piece_num,piece_dir,x,y,ov,at);
  return (!is_overlap(pos,piece_num,piece_dir,x,y)) && is_attached(pos,pl,piece_num,piece_dir,x,y);
}

// ピースを打つ場所があるかどうか
// param pos: 局面
// param pl: プレイヤー
bool can_put(struct position *pos, struct player *pl) {
#ifdef DEBUG
  printf("can_put? &pos:%x player_id:%d\n",pos,pl->id);
#endif
  int p,d,x,y;
  for(p=0;p<PIECE_NUM;++p) {
    // すでに使ったピースは関係ない
    if(pl->used[p]) continue;

    for(x=0;x<STAGE_SIZE;++x) {
      for(y=0;y<STAGE_SIZE;++y) {
        for(d=0;d<piece_dirs[p];++d) {
          // OKであれば打つ場所がある
          if(ok(pos,pl,p,d,x,y)) return true;
        }
      }
    }
  }
  return false;
}

// 使えるピースが残っているかを判定
bool is_left(struct position *pos, struct player *pl) {
#ifdef DEBUG
  printf("is_left? &pos:%x player_id:%d\n",pos,pl->id);
#endif
  int i;
  for(i=0;i<PIECE_NUM;++i) if(!pl->used[i]) return true;
  return false;
}

// 残っているマスの数を返す
// param pos: 盤面
// param pl: プレイヤー
int left(struct position *pos, struct player *pl) {
  int i,sum=0;
  for(i=0;i<PIECE_NUM;++i) {
    if(!pl->used[i]) sum += piece_sizes[i];
  }
  return sum;
}

// 置く
// param pos: 局面
// param pl: プレイヤー
// param piece_num: ピース番号
// param piece_dir: ピースの向き
// param pt: ピースを置く場所
void put(struct position *pos, struct player *pl, int piece_num, int piece_dir, int x, int y) {
  // for debug
#ifdef DEBUG
  printf("put &pos:%x player_id:%d piece_num:%d piece_dir:%d x:%d y:%d\n",pos,pl->id,piece_num,piece_dir,x,y);
#endif

  // 手番を保存しておく for undo
  pos->turns[pos->turn_pos].player = pl;
  pos->turns[pos->turn_pos].piece = piece_num;
  pos->turns[pos->turn_pos].dir = piece_dir;
  pos->turns[pos->turn_pos].pos.x = x;
  pos->turns[pos->turn_pos].pos.y = y;

  // 手番を適用する
  apply(pos,&pos->turns[pos->turn_pos++]);
}

// 手番を適用する
bool apply(struct position *pos, struct turn *t) {
#ifdef DEBUG
  printf("apply &pos:%x turn:%x\n",pos,t);
#endif
  struct player *pl = t->player;
  int piece_num = t->piece;
  int piece_dir = t->dir;
  int x = t->pos.x;
  int y = t->pos.y;
  int px,py,dx,dy,piece_status,pos_status;
  for(dx=0;dx<PIECE_SIZE;++dx) {
    for(dy=0;dy<PIECE_SIZE;++dy) {
      // 外はそのまま
      px = x + dx - PIECE_SIZE/2; py = y + dy - PIECE_SIZE/2;
      if(is_out(pos,px,py)) continue;

      // ピースのないマスは関係ない
      piece_status = get_piece(piece_num,piece_dir,dx,dy);
      if(piece_status == NONE) continue;

      // ピースを局面に反映する
      pos_status = get_data(pos,pl,px,py);
      if(pos_status != SET) set_data(pos,pl,px,py,piece_status);
    }
  }

  // 使ったピースをチェック
  pl->used[piece_num] = true;

  return true;
}

// 戻す
// param pos: 局面
bool undo(struct position *pos) {
  // 最後の手番を取得
  if(pos->turn_pos <= 0) return false;
  pos->turn_pos--;
  struct player *pl = pos->turns[pos->turn_pos].player;
  int piece_num = pos->turns[pos->turn_pos].piece;
  int piece_dir = pos->turns[pos->turn_pos].dir;
  int x = pos->turns[pos->turn_pos].pos.x;
  int y = pos->turns[pos->turn_pos].pos.y;

#ifdef DEBUG
  printf("undo &pos:%x player_id:%d piece_num:%d piece_dir:%d x:%d y%d:\n",pos,pl->id,piece_num,piece_dir,x,y);
#endif

  // undo
  int px,py,dx,dy;
  /*
  for(dx=0;dx<PIECE_SIZE;++dx) {
    for(dy=0;dy<PIECE_SIZE;++dy) {
      // ピースのないマスは関係ない
      if(get_piece(piece_num,piece_dir,dx,dy) == NONE) continue;
      // ピースをなかったことにする
      px = x + dx - PIECE_SIZE/2; py = y + dy - PIECE_SIZE/2;
      set_data(pos,pl,px,py,NONE);
    }
  }
  */
  // 使ったピースを戻す
  pl->used[piece_num] = false;

  // すべてのデータを初期化
  for(px=1;px<=STAGE_SIZE;++px) {
    for(py=0;py<=STAGE_SIZE;++py) {
      set_data(pos,pos->myself,px,py,NONE);
      set_data(pos,pos->opponent,px,py,NONE);
    }
  }
  for(x=0;x<STAGE_SIZE+2;++x) {
    set_data(pos,pos->myself,x,0,NG);
    set_data(pos,pos->myself,x,STAGE_SIZE+1,NG);
    set_data(pos,pos->opponent,x,0,NG);
    set_data(pos,pos->opponent,x,STAGE_SIZE+1,NG);
  }
  for(y=0;y<STAGE_SIZE+2;++y) {
    set_data(pos,pos->myself,0,y,NG);
    set_data(pos,pos->opponent,0,y,NG);
    set_data(pos,pos->myself,STAGE_SIZE+1,y,NG);
    set_data(pos,pos->opponent,STAGE_SIZE+1,y,NG);
  }
  set_data(pos,pos->myself,5,5,OK);
  set_data(pos,pos->opponent,10,10,OK);

  // undo前に復元するためにリプレイする(クズ)
  int turn;
  for(turn = 0;turn < pos->turn_pos;turn++) {
    apply(pos,&pos->turns[turn]);
  }

  // TODO: 戻しすぎてるみたい?

  return true;
}

// 勝敗を判定
// param pos: 盤面
// param pl: プレイヤー
bool win(struct position *pos, struct player *myself, struct player *opponent) {
  return (left(pos,myself) < left(pos,opponent) ? true : false);
}

// 局面を文字列にする
void to_str(struct position *pos, char *buf) {
  int x,y,bp=0;
  for(y=1;y<STAGE_SIZE+1;++y) {
    for(x=1;x<STAGE_SIZE+1;++x) {
      buf[bp++] = get_data(pos,pos->myself,x,y) + '0';
      buf[bp++] = get_data(pos,pos->opponent,x,y) + '0';
    }
  }
  buf[bp] = '\0';
}

// 局面の表示
// param pos: 局面
void print_pos(struct position *pos) {
  int x,y;
  for(y=0;y<STAGE_SIZE+2;++y) {
    for(x=0;x<STAGE_SIZE+2;++x) {
      char c = '.';
      if(is_out(pos,x,y)) c = '@';
      else if(get_data(pos,pos->myself,x,y) == SET) c = 'o';
      else if(get_data(pos,pos->opponent,x,y) == SET) c = 'x';
      else if(get_data(pos,pos->myself,x,y) == OK) c = '?';
      else if(get_data(pos,pos->myself,x,y) == NG) c = '!';
      printf("%c ",c);
    }
    printf("\n");
  }
}

