#include <string>
#include <map>

#include "blokus.h"

#define INF 1000000000

struct bound {
  int upper;
  int lower;
};

std::string to_s(struct position *pos) {
  std::string str = "";
  for(int x=1;x<=STAGE_SIZE;++x) {
    for(int y=1;y<=STAGE_SIZE;++y) {
      int m = get_data(pos,pos->myself,x,y);
      int o = get_data(pos,pos->opponent,x,y);
      str += '0' + m + (o << 2);
    }
  }
  return str;
}

// 置換表付きネガアルファ法で探索
// param pos: 局面
// param self: 自手
// param opp: 相手
// param parent_cost: 親ノードでの評価値
// param depth: 探索する深さ
// param alpha:
// param beta:
// param score_func:
// ret move:
// return: 評価値
int negalpha_with_table(struct position *pos, struct player *origin,
                        struct player *self, struct player *opp,
                        int depth, int alpha, int beta, struct turn *move,
                        int(*score_func)(struct position *,struct player *,struct player *)) {
#ifdef DEBUG
  printf("negmax pos:%x origin:%x self:%x opp:%x depth:%d move:%x func:%x\n",
         pos,origin,self,opp,depth,move,score_func);
#endif

  static std::map<std::string,struct bound> table;

  // 置換表にすでに入っていたらその値を利用する
  if(table.count(to_s(pos)) > 0) {
    int upper = table[to_s(pos)].upper;
    int lower = table[to_s(pos)].lower;

    // fail-high
    if(upper >= beta) {
      return upper;
    } else {
      beta = upper;
    }
    // fail-low
    if(lower <= alpha) {
      return lower;
    } else {
      alpha = lower;
    }
  }

  // 目標の深さが0になったら評価値をそのまま返す
  if(depth < 1) {
    return score_func(pos,self,opp);
  }

  // 評価の初期化
  int best_score = -INF;

  // すべての手を試す
  bool flag = true;
  for(int p=0;p<PIECE_NUM;++p) {
    for(int d=0;d<piece_dirs[p];++d) {
      for(int x=1;x<=STAGE_SIZE;++x) {
        for(int y=1;y<=STAGE_SIZE;++y) {
          // 置ける場所なら置く
          if(self->used[p]) continue;
          if(ok(pos,self,p,d,x,y)) {
            flag = false;

            // 置いてみて評価したら戻す
            put(pos,self,p,d,x,y);
            struct turn hoge;
            int tmp = -negalpha_with_table(pos,origin,opp,self,depth-1,-beta,-alpha,&hoge,score_func);
            undo(pos);

            // same as negmax
            if(tmp > best_score) {
              alpha = tmp;
              best_score = tmp;
              move->player = self;
              move->piece = p; move->dir = d;
              move->pos.x = x; move->pos.y = y;
            }
            // 枝刈り
            if(alpha >= beta) {
              return alpha;
            }
          }
        }
      }
    }
  }

  // 置ける場所がなかったら評価値を計算して返す
  if(flag) {
    return score_func(pos,self,opp);
  }

  return best_score;
}
