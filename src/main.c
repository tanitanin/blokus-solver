#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "blokus.h"
#include "counter.h"

int main(int argc, char *argv[]) {

  // 引数の処理
  int i=0;
  bool auto_put = false;
  bool turn_flag = false;
  int turn = 0, myturn = 0;
  int opp_strategy=-1, my_strategy=-1;
  int opp_depth=-1, my_depth=-1;
  while(--argc > 0 && ++i) {
    if(!strncmp(argv[i],"-a",4)) {
      auto_put = true;
    } else if(!strncmp(argv[i],"-t",4)) {
      if(argc--<1) return 0;
      sscanf(argv[++i],"%d",&myturn);
      turn_flag = true;
    } else if(!strncmp(argv[i],"-m",4)) {
      if(argc--<1) return 0;
      sscanf(argv[++i],"%d",&my_strategy);
    } else if(!strncmp(argv[i],"-o",4)) {
      if(argc--<1) return 0;
      sscanf(argv[++i],"%d",&opp_strategy);
    } else if(!strncmp(argv[i],"-md",5)) {
      if(argc--<1) return 0;
      sscanf(argv[++i],"%d",&my_depth);
    } else if(!strncmp(argv[i],"-od",5)) {
      if(argc--<1) return 0;
      sscanf(argv[++i],"%d",&opp_depth);
    }
  }

  char buf[1024];
  struct counter my_cnt,opp_cnt;

  // 始めるかどうか選択
  if(!auto_put) {
    printf("Would you like to start??[y/n]:");
    fflush(stdin);
    scanf("%s",&buf);
    if(strncmp(buf,"y",1024)) {
      printf("Quit\n");
      return 0;
    }
  }
  // ステージの初期化
  //printf("New game!\n");
  struct position stage;
  clear_position(&stage);
  //print_pos(&stage);

  // 先手後手を決める
  if(!turn_flag) {
    printf("You are the first??[y/n]:");
    fflush(stdin);
    scanf("%s",&buf);
    if(strncmp(buf,"y",1024)) {
      myturn = 1;
      printf("First:You Second:Opponent\n");
    } else {
      printf("First:Opponent Second:You\n");
    }
  }

  // 手動or自動
  if(!auto_put) {
    printf("You put automatically[y/n]:");
    fflush(stdin);
    scanf("%s",&buf);
    if(strncmp(buf,"y",1024)) {
      printf("Manual play\n");
    } else {
      auto_put = true;
      printf("Auto play\n");
    }
  }

  // 自分の戦略を決める
  if(my_strategy<0) {
    printf("Your strategy select:\n");
    printf("0: random\n");
    printf("1: minimax\n");
    printf("2: negmax\n");
    printf("3: alphabeta\n");
    printf("input:");
    fflush(stdin);
    scanf("%d",&my_strategy);
    if(my_strategy < 0) {
      printf("Unexpected strategy number\n");
      return 0;
    }
  }
  if(my_strategy>0 && my_depth<0) {
    printf("Input the depth of my search:");
    fflush(stdin);
    scanf("%d",&my_depth);
    printf("Your strategy:%d depth:%d\n",my_strategy,my_depth);
  }

  // 敵の戦略を決める
  if(opp_strategy<0) {
    printf("The opponent's strategy select:\n");
    printf("0: random\n");
    printf("1: minimax\n");
    printf("2: negmax\n");
    printf("3: alphabeta\n");
    printf("input:");
    fflush(stdin);
    scanf("%d",&opp_strategy);
    if(opp_strategy < 0) {
      printf("Unexpected strategy number\n");
      return 0;
    }
  }
  if(opp_strategy>0 && opp_depth<0) {
    printf("Input the depth of opponent's search:");
    fflush(stdin);
    scanf("%d",&opp_depth);
    printf("The opponent's strategy:%d depth:%d\n",opp_strategy,opp_depth);
  }

  // ゲームスタート
  int x,y,p,d;
  struct turn ai_best, my_best;
  int finish_count=0;
  while(true) {
    if(turn%2 == myturn) {
      // あなたの手番
      printf("It's your turn!(turn:%d)\n",turn+1);
      if(is_left(&stage,stage.myself)) {
        if(can_put(&stage,stage.myself)) {
          if(auto_put) {
            // 自動モード
            start_counter(&my_cnt);
            ai(&stage,stage.myself,stage.opponent,my_strategy,my_depth,&my_best);
            stop_counter(&my_cnt);
            p = my_best.piece; d = my_best.dir;
            x = my_best.pos.x; y = my_best.pos.y;
          } else {
            // 位置とピースの種類・方向を入力
            printf("Where do you put?\n");
            while(true) {
              printf("x:"); fflush(stdin); scanf("%d",&x);
              printf("y:"); fflush(stdin); scanf("%d",&y);
              printf("piece:"); fflush(stdin); scanf("%d",&p);
              printf("dir:"); fflush(stdin); scanf("%d",&d);
              if(is_out(&stage,x,y)) {
                printf("Unexpected coodinate.\n");
              } else if(p<0 || PIECE_NUM<p) {
                printf("Unexpected piece number.\n");
              } else if(stage.myself->used[p]) {
                printf("Already used.\n");
              } else if(d<0 || piece_dirs[p]<d) {
                printf("Unexpected direction.\n");
              } else if(ok(&stage,stage.myself,p,d,x,y)) {
                break;
              } else {
                printf("You cannot put there. Input again.\n");
              }
            }
          }
          if(turn>1) {
            put(&stage,stage.myself,p,d,x,y);
            printf("You put piece:%d dir:%d at (%d,%d). time:%lfsec\n",p,d,x,y,my_cnt.st);
            print_pos(&stage);
          }
          finish_count = 0;
        } else {
          printf("You cannot put any pieces.\n");
          finish_count++;
        }
      } else {
        printf("You've already put all pieces.\n");
        finish_count++;
      }
    } else {
      // 敵の手番
      printf("The opponent's turn!(turn:%d)\n",turn+1);
      if(is_left(&stage,stage.opponent)) {
        if(can_put(&stage,stage.opponent)) {
          // AIによって手番を実行
          start_counter(&opp_cnt);
          ai(&stage,stage.opponent,stage.myself,opp_strategy,opp_depth,&ai_best);
          stop_counter(&opp_cnt);
          if(turn>1) {
            put(&stage,stage.opponent,ai_best.piece,ai_best.dir,ai_best.pos.x,ai_best.pos.y);
            printf("The opponent puts piece:%d dir:%d at (%d,%d). time:%lfsec\n",
                ai_best.piece,ai_best.dir,ai_best.pos.x,ai_best.pos.y,opp_cnt.st);
            print_pos(&stage);
          }
          finish_count = 0;
        } else {
          printf("The opponent cannot put any pieces.\n");
          finish_count++;
        }
      } else {
        printf("The opponent has already put all pieces.\n");
        finish_count++;
      }
    }

    if(turn<2 && finish_count>0) break;
    if(turn==1) {
      put(&stage,stage.myself,p,d,x,y);
      printf("You put piece:%d dir:%d at (%d,%d). time:%lfsec\n",p,d,x,y,my_cnt.st);
      put(&stage,stage.opponent,ai_best.piece,ai_best.dir,ai_best.pos.x,ai_best.pos.y);
      printf("The opponent puts piece:%d dir:%d at (%d,%d). time:%lfsec\n",
          ai_best.piece,ai_best.dir,ai_best.pos.x,ai_best.pos.y,opp_cnt.st);
      print_pos(&stage);
    }

    if(finish_count>1) break;
    turn++;
    fflush(stdout);
  }

  printf("Finish!!\n");

  // 残っているピースを表示
  for(p=0;p<PIECE_NUM;++p) {
    printf("piece[%02d] you:%d opponent:%d\n",p,stage.myself->used[p],stage.opponent->used[p]);
  }

  // 勝敗を表示
  if(win(&stage,stage.myself,stage.opponent)) {
    printf("You win!!\n");
  } else if(win(&stage,stage.opponent,stage.myself)){
    printf("You lose...\n");
  } else {
    printf("Draw...\n");
  }
  fflush(stdout);

  return 0;
}
