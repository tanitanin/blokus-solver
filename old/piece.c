
#include "piece.h"

// 回転後のピースのx座標を取得
char get_rotate_x(char piece_dir, char x, char y) {
  return rotate[piece_dir][y][x] / PIECE_SIZE;
}

// 回転後のピースのy座標を取得
char get_rotate_y(char piece_dir, char x, char y) {
  return rotate[piece_dir][y][x] % PIECE_SIZE;
}

// 回転後のピースの状態を取得
char get_piece(char piece_num, char piece_dir, char x, char y) {
  return pieces[piece_num][get_rotate_x(piece_dir,x,y)][get_rotate_y(piece_dir,x,y)];
}

