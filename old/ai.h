#ifndef _AI_H_
#define _AI_H_

#include "blokus.h"

struct bound {
  int upper;
  int lower;
};

// AIのターンを実行する
void ai(struct position *pos, struct player *self, struct player *opp,
        int strategy, int depth, struct turn *best);

/* 評価関数 */
int score(struct position *pos, struct player *self, struct player *opp);

// ランダム
void random_move(struct position *pos, struct player *origin, struct turn *move);
// ミニマックス法で探索
int minimax(struct position *pos, struct player *origin,
            struct player *myself, struct player *opponent,
            int depth, struct turn *move,
            int(*score)(struct position *,struct player *,struct player *));
// ネガマックス法で探索
int negmax(struct position *pos, struct player *origin,
           struct player *myself, struct player *opponent,
           int depth, struct turn *move,
           int(*score)(struct position *,struct player *,struct player *));
// アルファベータ法で探索
int alphabeta(struct position *pos, struct player *origin,
              struct player *myself, struct player *opponent,
              int depth, int low, int high, struct turn *move,
              int(*score)(struct position *,struct player *,struct player *));

#endif /* _AI_H_ */
