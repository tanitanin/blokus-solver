#ifndef _HASH_H_
#define _HASH_H_

#include "blokus.h"
#include "ai.h"

#define HASH_SIZE 8*1024*1024

void htinit();
void insert(struct position *key, struct bound *value);
struct bound *find(struct position *key);
void remove(struct position *key);


#endif /* _HASH_H_ */
