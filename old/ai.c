
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "ai.h"

#define INF 10000000

// AIのターンを実行する
// param pos:
// param self:
// param opp:
// param strategy:
// param depth:
// ret best:
void ai(struct position *pos, struct player *self, struct player *opp,
        int strategy, int depth, struct turn *best) {
#ifdef DEBUG
  printf("ai pos:%x self:%x opp:%x strategy:%d depth:%d\n",pos,self,opp,strategy,depth);
#endif
  // 自手を決める
  // 
  switch(strategy) {
    case 1:
      minimax(pos,self,self,opp,depth,best,score);
      break;
    case 2:
      negmax(pos,self,self,opp,depth,best,score);
      break;
    case 3:
      alphabeta(pos,self,self,opp,depth,-INF,INF,best,score);
      break;
    default:
      random_move(pos,self,best);
      break;
  }
}

// その局面での評価値を計算する
// param pos: 局面
// param self:
// param opp:
// return: 評価値
int score(struct position *pos, struct player *self, struct player *opp) {
#ifdef DEBUG
  printf("score pos:%x self:%x opp:%x\n",pos,self,opp);
#endif
  int x,y,pos_data,dist_sum=0,ok_pos=0,ng_pos=0,none_pos=0;
  for(x=0;x<STAGE_SIZE;++x) {
    for(y=0;y<STAGE_SIZE;++y) {
      pos_data = get_data(pos,self,x,y);
      if(pos_data == SET) {
        dist_sum += (x-STAGE_SIZE/2)*(x-STAGE_SIZE/2) + (y-STAGE_SIZE/2)*(y-STAGE_SIZE/2);
      }
      if(pos_data == OK) {
        ok_pos++;
      }
      if(pos_data == NG) {
        ng_pos++;
      }
      if(pos_data == NONE) {
        none_pos++;
      }
    }
  }
  return -dist_sum + 10*ok_pos - 10*ng_pos - 20*none_pos + left(pos,opp) - left(pos,self);
}

// ランダム
void random_move(struct position *pos, struct player *origin, struct turn *move) {
  int p,d,x,y;
  srand(time(NULL));
  while(1) {
    p = rand() % 21;
    d = rand() % 8;
    x = rand() % STAGE_SIZE + 1;
    y = rand() % STAGE_SIZE + 1;
    if(origin->used[p] == false && ok(pos,origin,p,d,x,y)) {
      move->player = origin;
      move->piece = p;
      move->dir = d;
      move->pos.x = x;
      move->pos.y = y;
      break;
    }
  }
}

// ミニマックス法で探索
// param pos: 局面
// param self: 自手
// param opp: 相手
// param parent_cost: 親ノードでの評価値
// param depth: 探索する深さ
// param score_func:
// ret move:
// return: 評価値
int minimax(struct position *pos, struct player *origin,
            struct player *self, struct player *opp,
            int depth, struct turn *move,
            int(*score_func)(struct position *,struct player *,struct player *)) {
#ifdef DEBUG
  printf("minimax pos:%x origin:%x self:%x opp:%x depth:%d move:%x func:%x\n",
         pos,origin,self,opp,depth,move,score_func);
#endif
  // 目標の深さが0になったら評価値をそのまま返す
  if(depth < 1) {
    if(origin == self) return score_func(pos,self,opp);
    else return score_func(pos,opp,self);
  }

  // 評価の初期化
  int best_score = (self == origin ? 0 : INF);

  // すべての手を試す
  int p,d,x,y; bool flag = true;
  for(p=0;p<PIECE_NUM;++p) {
    for(d=0;d<piece_dirs[p];++d) {
      for(x=1;x<=STAGE_SIZE;++x) {
        for(y=1;y<=STAGE_SIZE;++y) {
          // 置ける場所なら置く
          if(self->used[p]) continue;
          if(ok(pos,self,p,d,x,y)) {
            flag = false;

            // 置いてみて評価したら戻す
            put(pos,self,p,d,x,y);
            struct turn hoge;
            int tmp = minimax(pos,origin,opp,self,depth-1,&hoge,score_func);
            undo(pos);

            // min or max
            if(self == origin) {
              if(tmp > best_score) {
                best_score = tmp;
                move->player = self;
                move->piece = p; move->dir = d;
                move->pos.x = x; move->pos.y = y;
              }
            } else {
              if(tmp < best_score) { 
                best_score = tmp;
                move->player = self;
                move->piece = p; move->dir = d;
                move->pos.x = x; move->pos.y = y;
              }
            }
          }
        }
      }
    }
  }

  // 置ける場所がなかったら評価値を計算して返す
  if(flag) {
    if(origin == self) return score_func(pos,self,opp);
    else return score_func(pos,opp,self);
  }

  return best_score;
}

// ネガマックス法で探索
// param pos: 局面
// param self: 自手
// param opp: 相手
// param parent_cost: 親ノードでの評価値
// param depth: 探索する深さ
// param score_func:
// ret move:
// return: 評価値
int negmax(struct position *pos, struct player *origin,
            struct player *self, struct player *opp,
            int depth, struct turn *move,
            int(*score_func)(struct position *,struct player *,struct player *)) {
#ifdef DEBUG
  printf("negmax pos:%x origin:%x self:%x opp:%x depth:%d move:%x func:%x\n",
         pos,origin,self,opp,depth,move,score_func);
#endif
  // 目標の深さが0になったら評価値をそのまま返す
  if(depth < 1) {
    return score_func(pos,self,opp);
  }

  // 評価の初期化
  int best_score = -INF;

  // すべての手を試す
  int p,d,x,y; bool flag = true;
  for(p=0;p<PIECE_NUM;++p) {
    for(d=0;d<piece_dirs[p];++d) {
      for(x=1;x<=STAGE_SIZE;++x) {
        for(y=1;y<=STAGE_SIZE;++y) {
          // 置ける場所なら置く
          if(self->used[p]) continue;
          if(ok(pos,self,p,d,x,y)) {
            flag = false;

            // 置いてみて評価したら戻す
            put(pos,self,p,d,x,y);
            struct turn hoge;
            int tmp = negmax(pos,origin,opp,self,depth-1,&hoge,score_func);
            undo(pos);

            // negmax
            if(-tmp > best_score) {
              best_score = -tmp;
              move->player = self;
              move->piece = p; move->dir = d;
              move->pos.x = x; move->pos.y = y;
            }
          }
        }
      }
    }
  }

  // 置ける場所がなかったら評価値を計算して返す
  if(flag) {
    return score_func(pos,self,opp);
  }

  return best_score;
}

// アルファベータ法で探索
// param pos: 局面
// param self: 自手
// param opp: 相手
// param parent_cost: 親ノードでの評価値
// param depth: 探索する深さ
// param low:
// param high:
// param score_func:
// ret move:
// return: 評価値
int alphabeta(struct position *pos, struct player *origin,
              struct player *self, struct player *opp,
              int depth, int low, int high, struct turn *move,
              int(*score_func)(struct position *,struct player *,struct player *)) {
#ifdef DEBUG
  printf("negmax pos:%x origin:%x self:%x opp:%x depth:%d move:%x func:%x\n",
         pos,origin,self,opp,depth,move,score_func);
#endif
  // 目標の深さが0になったら評価値をそのまま返す
  if(depth < 1) {
    return score_func(pos,self,opp);
  }

  // 評価の初期化
  int best_score = -INF;

  // すべての手を試す
  int p,d,x,y; bool flag = true;
  for(p=0;p<PIECE_NUM;++p) {
    for(d=0;d<piece_dirs[p];++d) {
      for(x=1;x<=STAGE_SIZE;++x) {
        for(y=1;y<=STAGE_SIZE;++y) {
          // 置ける場所なら置く
          if(self->used[p]) continue;
          if(ok(pos,self,p,d,x,y)) {
            flag = false;

            // 置いてみて評価したら戻す
            put(pos,self,p,d,x,y);
            struct turn hoge;
            int tmp = alphabeta(pos,origin,opp,self,depth-1,-high,-low,&hoge,score_func);
            undo(pos);

            // same as negmax
            if(-tmp > best_score) {
              low = -tmp;
              best_score = -tmp;
              move->player = self;
              move->piece = p; move->dir = d;
              move->pos.x = x; move->pos.y = y;
            }
            // 枝刈り
            if(low >= high) {
              return best_score;
            }
          }
        }
      }
    }
  }

  // 置ける場所がなかったら評価値を計算して返す
  if(flag) {
    return score_func(pos,self,opp);
  }

  return best_score;
}
