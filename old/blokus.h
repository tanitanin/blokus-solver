#ifndef _BLOKUS_H_
#define _BLOKUS_H_

#include <stdbool.h>
#include "piece.h"

#define OUT 3

// 座標を表す構造体
struct point {
  int x;
  int y;
};

// プレイヤーを表す構造体
struct player {
  int id;
  bool used[PIECE_NUM];
};

// 手番を表す構造体
struct turn {
  struct player *player;
  int piece;
  int dir;
  struct point pos;
};

// 局面を表す構造体
struct position {
  int data[(STAGE_SIZE+2)*(STAGE_SIZE+2)];
  struct player *myself;
  struct player *opponent;
  struct turn turns[PIECE_NUM*2+1];
  int turn_pos;
};

// 盤面の初期化
void clear_position(struct position *pos);
// 盤面の状態を設定,取得
void set_data(struct position *pos, struct player *self, int x, int y, int state);
int get_data(struct position *pos, struct player *self, int x, int y);
// 指定したマスが重なるかどうかを判定
bool is_overlap(struct position *pos, int piece_num, int piece_dir, int x, int y);
// 指定したピースが点で接しているかどうかを判定
bool is_attached(struct position *pos, struct player *self, int piece_num, int piece_dir, int x, int y);
// 指定したピースが打てるかどうかを判定
bool ok(struct position *pos, struct player *self, int piece_num, int piece_dir, int x, int y);
// ピースを打つ場所があるかどうかを判定
bool can_put(struct position *pos, struct player *self);
// 使えるピースが残っているかを判定
bool is_left(struct position *pos, struct player *self);
// 残っているマスの数を返す
int left(struct position *pos, struct player *self);
// ピースを局面に反映させる
void put(struct position *pos, struct player *self, int piece_num, int piece_dir, int x, int y);
// 手番を適用する
bool apply(struct position *pos, struct turn *t);
// 最後の手番を戻す
bool undo(struct position *pos);

// 勝敗を判定
bool win(struct position *pos, struct player *myself, struct player *opponent);

// 文字列にする
void to_str(struct position *pos, char *buf);
// 表示
void print_pos(struct position *pos);

#endif // _BLOKUS_H_

