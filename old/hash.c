#include "hash.h"
#include <search.h>

void htinit() {
  hcreate(HASH_SIZE);
}

void insert(struct position * key, struct bound * value) {
  char buf[STAGE_SIZE*STAGE_SIZE*2+1];
  to_str(key,buf);
  ENTRY e;
  e.key = strdup(buf);
  e.data = value;
  hsearch(e,ENTER);
}

struct bound * find(struct position * key) {
  char buf[STAGE_SIZE*STAGE_SIZE*2+1];
  to_str(key,buf);
  ENTRY e,*res;
  e.key = strdup(buf);
  res = hsearch(e,FIND);
  return (struct bound *)res->data;
}

void remove(struct position * key) {

}
